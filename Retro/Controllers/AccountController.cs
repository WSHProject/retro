﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Controllers
{
    public class AccountController : Controller
    {
        private readonly RetroContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IUser _user;

        public AccountController(RetroContext context, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IUser user)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _user = user;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }


        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(User model)
        {
            ViewData["requestForm"] = "register";

            if (ModelState.IsValid)
            {
                // create new identity user
                var user = new IdentityUser
                {
                    UserName = model.Login,
                    Email = model.EmailAddress
                };

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    // dodaj klienta do naszej bazy - synchronizacja 2 tabel
                    model.Active = true;
                    _context.Add(model);
                    await _context.SaveChangesAsync();

                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            ModelState.Clear();
            ModelState.AddModelError(string.Empty, "Podany login już instnieje!");

            return View("Index", model);
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(User model)
        {
            ViewData["requestForm"] = "login";

            // znajdź jego login
            var founduser = await _userManager.FindByNameAsync(model.Login);

            if (founduser != null)
            {
                // sprawdź w naszej wewnętrzbej bazie danych
                var exists = _context.Users.FirstOrDefault(u => u.Login == model.Login);
                if (exists == null)
                {
                    // nie istnieje w naszej bazie danych Retro
                    ModelState.Clear();
                    ModelState.AddModelError(string.Empty, "Użytkownik nie istnieje w bazie Retro!");
                    return View("Index", model);
                }
                else
                {
                    var result = await _signInManager.PasswordSignInAsync(model.Login, model.Password, model.RememberMe, false);

                    if (result.Succeeded)
                    {
                        exists.Active = true;
                        exists.RememberMe = model.RememberMe;
                        _context.Update(exists);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        // Nie mozna zalogować w signin manager
                        ModelState.Clear();
                        ModelState.AddModelError(string.Empty, "Nieprawidłowe hasło!");
                        return View("Index", model);
                    }
                }
            }

            ModelState.Clear();
            ModelState.AddModelError(string.Empty, "Wprowadzony Login nie istnieje!");
            return View("Index", model);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                // znajdź jego login
                var founduser = await _userManager.FindByNameAsync(User.Identity.Name);

                if (founduser != null)
                {
                    var user = _context.Users.FirstOrDefault(u => u.Login == User.Identity.Name);
                    if (user != null)
                    {
                        user.Active = false;
                        _context.Update(user);
                        await _context.SaveChangesAsync();
                    }
                    await _signInManager.SignOutAsync();
                    // HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                }
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
