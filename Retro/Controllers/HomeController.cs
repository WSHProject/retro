﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Models;

namespace Retro.Controllers
{
    public class HomeController : Controller
    {
        private readonly RetroContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IUser _user;


        public HomeController(RetroContext context, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IUser user)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _user = user;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Sessions()
        {
            var retroContext = _context.Sessions.Include(p => p.Projects);

            return View(await retroContext.ToListAsync());
        }

        public async Task<IActionResult> History(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var session = await _context.Sessions.Include(s => s.Projects).FirstOrDefaultAsync(m => m.Id == id);
            if (session == null)
            {
                return NotFound();
            }

            ViewData["CommentDetails"] = await _context.CommentDetails.ToListAsync();
            ViewData["Users"] = await _context.Users.Include(c => c.Comments).ThenInclude(t => t.CommentTypes).ToListAsync();

            return View(session);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Aboutus()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
