﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Retro.Data;
using Retro.Data.Models;

namespace Retro.Controllers
{
    public class SessionsController : Controller
    {
        private readonly RetroContext _context;

        public SessionsController(RetroContext context)
        {
            _context = context;
        }

        // GET: Sessions
        public async Task<IActionResult> Index()
        {
            var retroContext = _context.Sessions.Include(s => s.Projects);
            return View(await retroContext.ToListAsync());
        }

        // GET: Sessions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var session = await _context.Sessions
                .Include(s => s.Projects)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (session == null)
            {
                return NotFound();
            }

            return View(session);
        }

        // GET: Sessions/Create
        public IActionResult Create()
        {
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName");
            return View();
        }

        // POST: Sessions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,SessionName,NumberOfVotes,SessionDate,SessionTime,AccessKey,Anonym,ProjectId")] Session session)
        {
            if (ModelState.IsValid)
            {
                // initial - set current date, timespan and set as active
                session.SessionDate = DateTime.Now;
                session.RemainingTime = session.SessionTime;
                session.Active = true;
                _context.Add(session);
                await _context.SaveChangesAsync();

                // create comment types if not already created
                if (_context.CommentTypes.Any() == false)
                {
                    CommentType yesCommentType = new CommentType { TypeName = "plusy" };
                    CommentType noCommentType = new CommentType { TypeName = "minusy" };
                    await _context.AddAsync(yesCommentType);
                    await _context.AddAsync(noCommentType);
                    await _context.SaveChangesAsync();
                }

                // set scrum master for active session
                if (User.Identity.IsAuthenticated)
                {
                    // clear previous scrum master just in case and active state
                    foreach (var user in _context.Users)
                    {
                        user.ScrumMaster = false;
                        // set false - arbitrary
                        user.Active = false;
                    }

                    // find current user - set as SM and active
                    var u = _context.Users.FirstOrDefault(u => u.Login == User.Identity.Name);
                    if (u != null)
                    {
                        u.ScrumMaster = true;
                        u.Active = true;
                    }
                    _context.Update(u);
                    await _context.SaveChangesAsync();
                }
                return RedirectToAction("Index", "Comments");
            }
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName", session.ProjectId);
            return View(session);
        }

        // GET: Sessions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var session = await _context.Sessions.FindAsync(id);
            if (session == null)
            {
                return NotFound();
            }
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName", session.ProjectId);
            return View(session);
        }

        // POST: Sessions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SessionName,NumberOfVotes,SessionDate,SessionTime,AccessKey,ProjectId")] Session session)
        {
            if (id != session.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(session);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SessionExists(session.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName", session.ProjectId);
            return View(session);
        }

        // GET: Sessions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var session = await _context.Sessions
                .Include(s => s.Projects)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (session == null)
            {
                return NotFound();
            }

            return View(session);
        }

        // POST: Sessions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var session = await _context.Sessions.FindAsync(id);
            _context.Sessions.Remove(session);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SessionExists(int id)
        {
            return _context.Sessions.Any(e => e.Id == id);
        }
    }
}
