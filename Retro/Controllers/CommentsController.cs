﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Retro.Data;
using Retro.Data.Models;

namespace Retro.Controllers
{
    public class CommentsController : Controller
    {
        private readonly RetroContext _context;

        public CommentsController(RetroContext context)
        {
            _context = context;
        }

        // GET: Comments
        public async Task<IActionResult> Index()
        {

            // dodaj także do SessionDetail aktualnego zalogowanego usera który tu wszedł
            try
            {
                foreach (var s in _context.Sessions)
                {
                    if (s.Active)
                    {
                        foreach (var u in _context.Users)
                        {
                            // czy sesja jest aktywna
                            if (u.Active)
                            {
                                // dodaj wpis do session details tylko jeżeli jeszcze nie istnieje
                                if (_context.SessionDetails.Any(d => d.UserId == u.Id && d.SessionId == s.Id) == false)
                                {
                                    var sdetail = new SessionDetail { UserId = u.Id, SessionId = s.Id };
                                    _context.Add(sdetail);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {

            }
            // clear authenticated users flag in our db and request confirmation

            await _context.SaveChangesAsync();
            // var retroContext = _context.Comments.Include(c => c.CommentTypes).Include(c => c.Users);

            ViewData["CommentModel"] = new Comment();
            ViewData["ActiveSession"] = await _context.Sessions.FirstOrDefaultAsync(s => s.Active == true);
            ViewData["CommentDetails"] = await _context.CommentDetails.ToListAsync();
            if (User.Identity.IsAuthenticated)
            {
                // find current user in activeusers list
                var u = await _context.Users.FirstOrDefaultAsync(u => u.Login == User.Identity.Name);
                if (u != null) ViewData["CurrentUser"] = u;
            }
            ViewData["CommentTypeId"] = new SelectList(_context.CommentTypes, "Id", "TypeName");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Login");

            // zmiana modelu na users
            var retroContext = _context.Users.Include(c => c.Comments).ThenInclude(t => t.CommentTypes);
            return View(await retroContext.ToListAsync());
        }

        [HttpPost]
        public async Task<IActionResult> Index(Session model)
        {
            // znajdź sesję
            var foundsession = await _context.Sessions.FirstOrDefaultAsync(s => s.AccessKey == model.AccessKey);

            if (foundsession != null)
            {
                return RedirectToAction("Index", "Comments");
            }

            ModelState.Clear();
            ModelState.AddModelError(string.Empty, "Błędne hasło do sesji!");
            return RedirectToAction("Sessions", "Home");
        }

        // GET: Comments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comments
                .Include(c => c.CommentTypes)
                .Include(c => c.Users)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // GET: Comments/Create
        public IActionResult Create()
        {
            ViewData["CommentTypeId"] = new SelectList(_context.CommentTypes, "Id", "TypeName");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Login");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Content,Value,Anonym,UserId,CommentTypeId")] Comment comment)
        {
            // dodaj wartości domyślne
            comment.Value = 0;
            comment.UserId = _context.Users.FirstOrDefault(u => u.Login == User.Identity.Name).Id;
            if (ModelState.IsValid)
            {
                _context.Add(comment);
                await _context.SaveChangesAsync();

                // dodaj także do CommentDetail
                try
                {
                    foreach (var s in _context.Sessions)
                    {
                        if (s.Active)
                        {
                            var cdetail = new CommentDetail { CommentId = comment.Id, SessionId = s.Id };
                            _context.Add(cdetail);
                            break;
                        }
                    }
                }
                catch
                {

                }
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommentTypeId"] = new SelectList(_context.CommentTypes, "Id", "TypeName", comment.CommentTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Login", comment.UserId);

            return RedirectToAction(nameof(Index));
            // return View(comment);
        }

        // GET: Comments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comments.FindAsync(id);
            if (comment == null)
            {
                return NotFound();
            }
            ViewData["CommentTypeId"] = new SelectList(_context.CommentTypes, "Id", "TypeName", comment.CommentTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Login", comment.UserId);
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Content,Value,Anonym,UserId,CommentTypeId")] Comment comment)
        {
            if (id != comment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(comment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommentExists(comment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommentTypeId"] = new SelectList(_context.CommentTypes, "Id", "TypeName", comment.CommentTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Login", comment.UserId);
            return View(comment);
        }

        // GET: Comments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comments
                .Include(c => c.CommentTypes)
                .Include(c => c.Users)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var comment = await _context.Comments.FindAsync(id);
            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CommentExists(int id)
        {
            return _context.Comments.Any(e => e.Id == id);
        }

        [ResponseCache(Duration = 2, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task <PartialViewResult> CommentsUpdate()
        {
            await _context.SaveChangesAsync();

            ViewData["CommentModel"] = new Comment();
            ViewData["ActiveSession"] = await _context.Sessions.FirstOrDefaultAsync(s => s.Active == true);
            ViewData["CommentDetails"] = await _context.CommentDetails.ToListAsync();
            ViewData["CommentTypeId"] = new SelectList(_context.CommentTypes, "Id", "TypeName");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Login");

            // zmiana modelu na users
            var retroContext = _context.Users.Include(c => c.Comments).ThenInclude(t => t.CommentTypes);
            return PartialView("Partialc", await retroContext.ToListAsync());
        }

        [ResponseCache(Duration = 2, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task <PartialViewResult> UsersUpdate()
        {
            await _context.SaveChangesAsync();

            ViewData["CommentModel"] = new Comment();
            ViewData["ActiveSession"] = await _context.Sessions.FirstOrDefaultAsync(s => s.Active == true);
            ViewData["CommentDetails"] = await _context.CommentDetails.ToListAsync();
            ViewData["CommentTypeId"] = new SelectList(_context.CommentTypes, "Id", "TypeName");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Login");

            // zmiana modelu na users
            var retroContext = _context.Users.Include(c => c.Comments).ThenInclude(t => t.CommentTypes);
            return PartialView("Partialu", await retroContext.ToListAsync());
        }

        // call every 1000ms
        public async Task<string> UsersSynchronize()
        {
            string res;

            // end session if scrum master ended session
            res = "stop";

            var s = await _context.Sessions.FirstOrDefaultAsync(s => s.Active == true);

            // session still active
            if (s != null)
            {
                res = "ok";

                if (User.Identity.IsAuthenticated)
                {
                    // find current user in activeusers list
                    var u = await _context.Users.FirstOrDefaultAsync(u => u.Login == User.Identity.Name);
                    if (u != null)
                    {
                        // if current user is authenticated switch flag in active users to true
                        if (u.Active == false)
                        {
                            u.Active = true;
                            _context.Update(u);
                        }

                        // instruct scrum master to decrease counter
                        if (u.ScrumMaster)
                        {
                            s.RemainingTime = s.RemainingTime.Subtract(new TimeSpan(0, 0, 1));

                            // is it the end?
                            TimeSpan zero = new TimeSpan(0, 0, 0);
                            if (s.RemainingTime == zero)
                            {
                                s.RemainingTime = new TimeSpan(0, 1, 0);
                                // to next scene
                                res = "move";
                            }
                            _context.Update(s);
                        }

                        // instruct clients to refresh Index for mode 1 (głosowanie) and mode 2 (wyniki) at 1min interval start
                        if (u.ScrumMaster == false)
                        {
                            // refresh clients in mode 1 (głosowanie) when time left is 59 seconds
                            if (s.Mode == 1 && s.RemainingTime == new TimeSpan(0, 0, 59))
                            {
                                res = "refreshclients";
                            }
                        }

                    }
                }
            }
            await _context.SaveChangesAsync();

            return res;
        }

        // executed only by scrum master
        public async Task<IActionResult> Move()
        {
            int cmode = 0;
            // move session to next phase
            var s = await _context.Sessions.FirstOrDefaultAsync(s => s.Active == true);

            if (s != null)
            {
                s.Mode++;
                // ustaw zegar na 1 min dla głosowania jeżeli ręcznie zakończonqa sesja
                if (s.Mode == 1) s.RemainingTime = new TimeSpan(0, 1, 0);
                cmode = s.Mode;
                _context.Update(s);

                // add votes to users
                if (s.Mode == 1)
                {
                    s.RemainingTime = new TimeSpan(0, 1, 0);
                    if (User.Identity.IsAuthenticated)
                    {
                        // find current user in users list
                        var u = await _context.Users.FirstOrDefaultAsync(u => u.Login == User.Identity.Name);
                        if (u != null && u.ScrumMaster)
                        {
                            foreach (var us in _context.Users)
                            {
                                // add votes to all users
                                us.Votes = s.NumberOfVotes;
                                _context.Update(us);
                            }
                        }
                    }
                }
            }
            await _context.SaveChangesAsync();

            if (cmode >= 3) return await Stop();
            else return RedirectToAction("Index", "Comments");
        }

        public async Task<IActionResult> Stop()
        {
            // make session inactive
            foreach (var s in _context.Sessions)
            {
                if (s.Active)
                {
                    s.Active = false;
                    s.Mode = 0;
                }
                _context.Update(s);
            }

            // make all scrummasters inactive
            foreach (var user in _context.Users)
            {
                if (user.ScrumMaster) user.ScrumMaster = false;
                _context.Update(user);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Sessions", "Home");
        }

        public async Task<IActionResult> Vote(int? Id)
        {

            if (User.Identity.IsAuthenticated)
            {
                // find current user in users list
                var u = await _context.Users.FirstOrDefaultAsync(u => u.Login == User.Identity.Name);
                if (u != null)
                {
                    // take vote
                    if (u.Votes > 0)
                    {
                        // use 1 vote
                        u.Votes--;
                        _context.Update(u);

                        // add it to comment
                        var com = await _context.Comments.FirstOrDefaultAsync(com => com.Id == Id);
                        if (com != null)
                        {
                            com.Value++;
                            _context.Update(com);
                        }

                    }

                }
                _context.SaveChanges();

            }
            return RedirectToAction("Index", "Comments");
        }
    }
}

