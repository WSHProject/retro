﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Retro.Migrations
{
    public partial class anonymdlasesji : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Anonym",
                table: "Sessions",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Anonym",
                table: "Sessions");
        }
    }
}
