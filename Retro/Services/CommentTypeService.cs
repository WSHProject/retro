﻿using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Services
{
    public class CommentTypeService : ICommentType
    {
        private RetroContext _context;

        public CommentTypeService(RetroContext context)
        {
            _context = context;
        }


        public CommentType GetById(int id)
        {
            return _context.CommentTypes.FirstOrDefault(CommentType => CommentType.Id == id);
        }
        public IEnumerable<CommentType> GetAll()
        {
            return _context.CommentTypes;
        }
        public void Add(CommentType newCommentType)
        {
            _context.Add(newCommentType);
            _context.SaveChanges();
        }
        public void Remove(int id)
        {
            CommentType newCommentType = _context.CommentTypes.FirstOrDefault(CommentType => CommentType.Id == id);
            _context.Remove(newCommentType);
            _context.SaveChanges();
        }


        public string GetTypeName(int id)
        {
            return GetById(id).TypeName;
        }


        public IEnumerable<Comment> GetComments(int id)
        {
            // return all Comments where Comment.CommentTypeId match the CommentType id given
            if (_context.CommentTypes.Any(CommentType => CommentType.Id == id))
            {
                return _context.Comments.AsEnumerable().Where(Comment => Comment.CommentTypeId == id);
            }
            return null;
        }

    }
}
