﻿using Microsoft.EntityFrameworkCore;
using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Services
{
    public class SessionDetailService : ISessionDetail
    {
        private RetroContext _context;

        public SessionDetailService(RetroContext context)
        {
            _context = context;
        }


        public SessionDetail GetById(int UserId, int SessionId)
        {
            return _context.SessionDetails.FirstOrDefault(SessionDetail => SessionDetail.UserId == UserId && SessionDetail.SessionId == SessionId);
        }
        public IEnumerable<SessionDetail> GetAll()
        {
            return _context.SessionDetails.Include(SessionDetail => SessionDetail.Users).Include(SessionDetail => SessionDetail.Sessions);
        }
        public void Add(SessionDetail newSessionDetail)
        {
            _context.Add(newSessionDetail);
            _context.SaveChanges();
        }
        public void Remove(int UserId, int SesionId)
        {
            SessionDetail newSessionDetail = _context.SessionDetails.FirstOrDefault(SessionDetail => SessionDetail.UserId == UserId && SessionDetail.SessionId == SesionId);
            _context.Remove(newSessionDetail);
            _context.SaveChanges();
        }

    }
}
