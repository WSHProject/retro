﻿using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Services
{
    public class UserService : IUser
    {
        private RetroContext _context;

        public UserService(RetroContext context)
        {
            _context = context;
        }


        public User GetById(int id)
        {
            return _context.Users.FirstOrDefault(User => User.Id == id);
        }
        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }
        public void Add(User newUser)
        {
            _context.Add(newUser);
            _context.SaveChanges();
        }
        public void Remove(int id)
        {
            User newUser = _context.Users.FirstOrDefault(User => User.Id == id);
            _context.Remove(newUser);
            _context.SaveChanges();
        }


        public string GetLogin(int id)
        {
            return GetById(id).Login;
        }
        public string GetPassword(int id)
        {
            return GetById(id).Password;
        }
        public string GetFirstName(int id)
        {
            return GetById(id).FirstName;
        }
        public string GetLastName(int id)
        {
            return GetById(id).LastName;
        }
        public string GetEmailAddress(int id)
        {
            return GetById(id).EmailAddress;
        }
        public bool GetActive(int id)
        {
            return GetById(id).Active;
        }
        public void SetActive(int id, bool status)
        {
            _context.Users.FirstOrDefault(User => User.Id == id).Active = status;
            _context.SaveChanges();
        }

        public void SetPassword(int id, string password)
        {
            _context.Users.FirstOrDefault(User => User.Id == id).Password = password;
            _context.SaveChanges();
        }
        public string GetAvatarUrl(int id)
        {
            return GetById(id).AvatarUrl;
        }


        public IEnumerable<Comment> GetComments(int id)
        {
            // return all Comments where Comment.UserId match the User id given
            if (_context.Users.Any(User => User.Id == id))
            {
                return _context.Comments.AsEnumerable().Where(Comment => Comment.UserId == id);
            }
            return null;
        }
        public IEnumerable<SessionDetail> GetSessionDetails(int id)
        {
            // return all SessionDetails where SessionDetail.UserId match the User id given
            if (_context.Users.Any(User => User.Id == id))
            {
                return _context.SessionDetails.AsEnumerable().Where(SessionDetail => SessionDetail.UserId == id);
            }
            return null;
        }

    }
}
