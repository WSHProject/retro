﻿using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Services
{
    public class ProjectService : IProject
    {
        private RetroContext _context;

        public ProjectService(RetroContext context)
        {
            _context = context;
        }

        public Project GetById(int id)
        {
            return _context.Projects.FirstOrDefault(Project => Project.Id == id);
        }
        public IEnumerable<Project> GetAll()
        {
            return _context.Projects;
        }
        public void Add(Project newProject)
        {
            _context.Add(newProject);
            _context.SaveChanges();
        }
        public void Remove(int id)
        {
            Project newProject = _context.Projects.FirstOrDefault(Project => Project.Id == id);
            _context.Remove(newProject);
            _context.SaveChanges();
        }

        public string GetProjectName(int id)
        {
            return GetById(id).ProjectName;
        }

        public IEnumerable<Session> GetSessions(int id)
        {
            // return all Sessions where Session.ProjectId match the Project id given
            if (_context.Projects.Any(Project => Project.Id == id))
            {
                return _context.Sessions.AsEnumerable().Where(Session => Session.ProjectId == id);
            }
            return null;
        }

    }
}
