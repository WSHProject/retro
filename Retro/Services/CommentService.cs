﻿using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Services
{
    public class CommentService : IComment
    {
        private RetroContext _context;

        public CommentService(RetroContext context)
        {
            _context = context;
        }


        public Comment GetById(int id)
        {
            return _context.Comments.FirstOrDefault(Comment => Comment.Id == id);
        }
        public IEnumerable<Comment> GetAll()
        {
            return _context.Comments;
        }
        public void Add(Comment newComment)
        {
            _context.Add(newComment);
            _context.SaveChanges();
        }
        public void Remove(int id)
        {
            Comment newComment = _context.Comments.FirstOrDefault(Comment => Comment.Id == id);
            _context.Remove(newComment);
            _context.SaveChanges();
        }


        public string GetContent(int id)
        {
            return GetById(id).Content;
        }
        public int GetValue(int id)
        {
            return GetById(id).Value;
        }
        public bool Anonym(int id)
        {
            return GetById(id).Anonym;
        }


        public CommentType GetCommentType(int id)
        {
            CommentType newCommentType;

            // get comment type id for current comment
            int CommentTypeId = GetById(id).CommentTypeId;

            // return first comment type that matches the comment type id on comments table
            try
            {
                newCommentType = _context.CommentTypes.FirstOrDefault(CommentType => CommentType.Id == CommentTypeId);
            }
            catch
            {
                newCommentType = null;
            }
            return newCommentType;
        }
        public User GetUser(int id)
        {
            // return GetById(id).UserId;
            User newUser;

            // get user id for current comment
            int UserId = GetById(id).UserId;

            // return first user that matches the user id on users table
            try
            {
                newUser = _context.Users.FirstOrDefault(User => User.Id == UserId);
            }
            catch
            {
                newUser = null;
            }
            return newUser;
        }
        public IEnumerable<CommentDetail> GetCommentDetails(int id)
        {
            // return all CommentDetails where CommentDetail.CommentId match the comment id given
            if (_context.Comments.Any(Comment => Comment.Id == id))
            {
                return _context.CommentDetails.AsEnumerable().Where(CommentDetail => CommentDetail.CommentId == id);
            }
            return null;
        }

    }
}
