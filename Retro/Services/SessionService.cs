﻿using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Services
{
    public class SessionService : ISession
    {
        private RetroContext _context;

        public SessionService(RetroContext context)
        {
            _context = context;
        }


        public Session GetById(int id)
        {
            return _context.Sessions.FirstOrDefault(Session => Session.Id == id);
        }
        public IEnumerable<Session> GetAll()
        {
            return _context.Sessions;
        }
        public void Add(Session newSession)
        {
            _context.Add(newSession);
            _context.SaveChanges();
        }
        public void Remove(int id)
        {
            Session newSession = _context.Sessions.FirstOrDefault(Session => Session.Id == id);
            _context.Remove(newSession);
            _context.SaveChanges();
        }


        public string GetSessionName(int id)
        {
            return GetById(id).SessionName;
        }
        public int GetNumberOfVotes(int id)
        {
            return GetById(id).NumberOfVotes;
        }
        public void SetNumberOfVotes(int id, int votes)
        {
            _context.Sessions.FirstOrDefault(Session => Session.Id == id).NumberOfVotes = votes;
            _context.SaveChanges();
        }
        public string GetAccessKey(int id)
        {
            return GetById(id).AccessKey;
        }
        public void SetAccessKey(int id, string accesskey)
        {
            _context.Sessions.FirstOrDefault(Session => Session.Id == id).AccessKey = accesskey;
            _context.SaveChanges();
        }
        public DateTime GetSessionDate(int id)
        {
            return GetById(id).SessionDate;
        }

        public TimeSpan GetSessionTime(int id)
        {
            return GetById(id).SessionTime;
        }
        public bool IsActive(int id)
        {
            return GetById(id).Active;
        }
        public void SetActive(int id)
        {
            _context.Sessions.FirstOrDefault(Session => Session.Id == id).Active = true;
            _context.SaveChanges();
        }
        public void SetInactive(int id)
        {
            _context.Sessions.FirstOrDefault(Session => Session.Id == id).Active = false;
            _context.SaveChanges();
        }

        public Project GetProject(int id)
        {
            Project newProject;

            // get project id for current session
            int ProjectId = GetById(id).ProjectId;

            // return first project that matches the project id on projects table
            try
            {
                newProject = _context.Projects.FirstOrDefault(Project => Project.Id == ProjectId);
            }
            catch
            {
                newProject = null;
            }
            return newProject;
        }
        public IEnumerable<SessionDetail> GetSessionDetails(int id)
        {
            // return all SessionDetails where SessionDetail.SessionId match the session id given
            if (_context.Sessions.Any(Session => Session.Id == id))
            {
                return _context.SessionDetails.AsEnumerable().Where(SessionDetail => SessionDetail.SessionId == id);
            }
            return null;
        }
        public IEnumerable<CommentDetail> GetCommentDetails(int id)
        {
            // return all CommentDetails where CommentDetail.SessionId match the session id given
            if (_context.Sessions.Any(Session => Session.Id == id))
            {
                return _context.CommentDetails.AsEnumerable().Where(CommentDetail => CommentDetail.CommentId == id);
            }
            return null;
        }

    }
}
