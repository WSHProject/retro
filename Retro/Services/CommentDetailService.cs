﻿using Microsoft.EntityFrameworkCore;
using Retro.Data;
using Retro.Data.Interfaces;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Services
{
    public class CommentDetailService : ICommentDetail
    {
        private RetroContext _context;

        public CommentDetailService(RetroContext context)
        {
            _context = context;
        }

        public CommentDetail GetById(int CommentId, int SessionId)
        {
            return _context.CommentDetails.FirstOrDefault(CommentDetail => CommentDetail.CommentId == CommentId && CommentDetail.SessionId == SessionId);
        }
        public IEnumerable<CommentDetail> GetAll()
        {
            return _context.CommentDetails.Include(CommentDetail => CommentDetail.Comments).Include(CommentDetail => CommentDetail.Sessions);
        }
        public void Add(CommentDetail newCommentDetail)
        {
            _context.Add(newCommentDetail);
            _context.SaveChanges();
        }
        public void Remove(int CommentId, int SesionId)
        {
            CommentDetail newCommentDetail = _context.CommentDetails.FirstOrDefault(CommentDetail => CommentDetail.CommentId == CommentId && CommentDetail.SessionId == SesionId);
            _context.Remove(newCommentDetail);
            _context.SaveChanges();
        }

    }
}
