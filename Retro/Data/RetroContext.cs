﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data
{
    public class RetroContext : IdentityDbContext
    {
        public RetroContext(DbContextOptions options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<CommentType> CommentTypes { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<CommentDetail> CommentDetails { get; set; }
        public DbSet<SessionDetail> SessionDetails { get; set; }
        public DbSet<Project> Projects { get; set; }

        // fluent API - utwórz złożony klucz z dwóch kluczy obcych
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Composite primary key for SessionDetail
            modelBuilder.Entity<SessionDetail>().HasKey(s => new { s.UserId, s.SessionId });

            // 2 foreign keys
            modelBuilder.Entity<SessionDetail>()
                .HasOne(u => u.Users)
                .WithMany()
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<SessionDetail>()
                .HasOne(s => s.Sessions)
                .WithMany()
                .HasForeignKey(s => s.SessionId);

            // Composite primary key for CommentDetail
            modelBuilder.Entity<CommentDetail>().HasKey(c => new { c.CommentId, c.SessionId });

            // 2 foreign keys
            modelBuilder.Entity<CommentDetail>()
                .HasOne(c => c.Comments)
                .WithMany()
                .HasForeignKey(c => c.CommentId);

            modelBuilder.Entity<CommentDetail>()
                .HasOne(s => s.Sessions)
                .WithMany()
                .HasForeignKey(s => s.SessionId);
        }
    }
}
