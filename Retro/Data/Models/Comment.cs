﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Models
{
    [Table("Comments")]
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Treść")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [Required]
        public int Value { get; set; }
        [Display(Name = "Anonimowo")]
        public bool Anonym { get; set; }

        [ForeignKey("Users")]
        public int UserId { get; set; }
        [ForeignKey("CommentTypes")]
        [Display(Name = "Rodzaj komentarza")]
        public int CommentTypeId { get; set; }

        // Many comments for 1 user (n-1)
        public virtual User Users { get; set; }
        // Many comments for 1 comment type (n-1)
        public virtual CommentType CommentTypes { get; set; }
    }
}
