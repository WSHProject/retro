﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Models
{
    [Table("SessionDetails")]
    public class SessionDetail
    {
        [Key, ForeignKey("Sessions")]
        public int SessionId { get; set; }
        [Key, ForeignKey("Users")]
        public int UserId { get; set; }

        public virtual Session Sessions { get; set; }
        public virtual User Users { get; set; }
    }
}
