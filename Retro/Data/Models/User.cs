﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Login jest wymagany")]
        public string Login { get; set; }
        
        [Required(ErrorMessage = "Hasło jest wymagane")]
        [Display(Name = "Hasło")]
        [StringLength(255, ErrorMessage = "Hasło musi mieć 6 znaków w tym 1 cyfrę", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Potwierdzenie hasła jest wymagane")]
        [Display(Name = "Potwierdź Hasło")]
        [StringLength(255, ErrorMessage = "Hasło musi mieć 6 znaków w tym 1 cyfrę", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Hasło i potwierdzenie hasła się różnią")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Imię jest wymagane")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Nazwisko jest wymagane")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Email jest wymagany")]
        [Display(Name = "Adres E-mail")]
        [EmailAddress(ErrorMessage = "To nie jest prawidłowy adres E-mail")]
        public string EmailAddress { get; set; }
        public bool Active { get; set; }
        public bool ScrumMaster { get; set; }
        public int Votes { get; set; }
        [Display(Name = "Zapamiętaj mnie")]
        public bool RememberMe { get; set; }
        [DataType(DataType.ImageUrl)]
        public string AvatarUrl { get; set; }

        // this user has many comments (1-n)
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
