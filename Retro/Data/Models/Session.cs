﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Models
{
    [Table("Sessions")]
    public class Session
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Nazwa sesji jest wymagana")]
        [Display(Name = "Nazwa sesji")]
        public string SessionName { get; set; }
        [Required(ErrorMessage = "Proszę wybrać ilość głosów")]
        [Display(Name = "Ilość głosów")]
        public int NumberOfVotes { get; set; }
        [Required]
        [Display(Name = "Data sesji")]
        [DataType(DataType.Date)]
        public DateTime SessionDate { get; set; }
        [Required(ErrorMessage = "Proszę wybrać limit czasu")]
        [Display(Name = "Limit czasu")]
        [DataType(DataType.Time)]
        public TimeSpan SessionTime { get; set; }
        [DataType(DataType.Time)]
        public TimeSpan RemainingTime { get; set; }
        // access key for new users that want to register
        [Required(ErrorMessage = "Proszę wpisać klucz dostępu")]
        [Display(Name = "Klucz dostępu")]
        public string AccessKey { get; set; }
        public bool Active { get; set; }
        // 0 = add comments, 1 = vote, 2 = results
        public int Mode { get; set; }

        [Display(Name = "Anonimowa")]
        public bool Anonym { get; set; }
        [ForeignKey("Projects")]
        [Display(Name = "Projekt obejmujący sesję")]
        public int ProjectId { get; set; }

        // Many Sessions for 1 Comment Detail (n-1)
        [Display(Name = "Projekt")]
        public virtual Project Projects { get; set; }
    }
}
