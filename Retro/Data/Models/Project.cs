﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Models
{
    [Table("Projects")]
    public class Project
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Proszę podać nazwę projektu")]
        [Display(Name = "Nazwa Projektu")]
        public string ProjectName { get; set; }

        // 1 Project for many sessions (1-n)
        public virtual ICollection<Session> Sessions { get; set; }
    }
}
