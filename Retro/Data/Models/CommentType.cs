﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Models
{
    [Table("CommentTypes")]
    public class CommentType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string TypeName { get; set; }

        // 1 comment type for many comments (1-n)
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
