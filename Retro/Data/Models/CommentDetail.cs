﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Models
{
    [Table("CommentDetails")]
    public class CommentDetail
    {
        [Key, ForeignKey("Sessions")]
        public int SessionId { get; set; }
        [Key, ForeignKey("Comments")]
        public int CommentId { get; set; }

        public virtual Session Sessions { get; set; }
        public virtual Comment Comments { get; set; }
    }
}
