﻿using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Interfaces
{
    public interface ISessionDetail
    {
        SessionDetail GetById(int UserId, int SessionId);
        IEnumerable<SessionDetail> GetAll();
        void Add(SessionDetail newSessionDetail);
        void Remove(int UserId, int SesionId);
    }
}
