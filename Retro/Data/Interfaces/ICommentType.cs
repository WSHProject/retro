﻿using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Interfaces
{
    public interface ICommentType
    {
        CommentType GetById(int id);
        IEnumerable<CommentType> GetAll();
        void Add(CommentType newComment);
        void Remove(int id);

        string GetTypeName(int id);

        // 1 to many - return all comments for this comment detail
        IEnumerable<Comment> GetComments(int id);
    }
}
