﻿using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Interfaces
{
    public interface IComment
    {
        Comment GetById(int id);
        IEnumerable<Comment> GetAll();
        void Add(Comment newComment);
        void Remove(int id);

        string GetContent(int id);
        int GetValue(int id);
        bool Anonym(int id);

        // many to 1
        CommentType GetCommentType(int id);
        User GetUser(int id);

        // 1 to many - return all comment details for this comment
        IEnumerable<CommentDetail> GetCommentDetails(int id);
    }
}
