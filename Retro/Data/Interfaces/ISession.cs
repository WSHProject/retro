﻿using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Interfaces
{
    public interface ISession
    {
        Session GetById(int id);
        IEnumerable<Session> GetAll();
        void Add(Session newSession);
        void Remove(int id);

        string GetSessionName(int id);
        int GetNumberOfVotes(int id);
        void SetNumberOfVotes(int id, int votes);
        string GetAccessKey(int id);
        void SetAccessKey(int id, string accesskey);
        DateTime GetSessionDate(int id);
        TimeSpan GetSessionTime(int id);
        bool IsActive(int id);
        void SetActive(int id);
        void SetInactive(int id);

        // many to 1
        Project GetProject(int id);

        // 1 to many - return all session details for this comment
        IEnumerable<SessionDetail> GetSessionDetails(int id);
        // 1 to many - return all comment details for this comment
        IEnumerable<CommentDetail> GetCommentDetails(int id);
    }
}
