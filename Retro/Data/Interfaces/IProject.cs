﻿using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Interfaces
{
    public interface IProject
    {
        Project GetById(int id);
        IEnumerable<Project> GetAll();
        void Add(Project newProject);
        void Remove(int id);

        string GetProjectName(int id);

       // 1 to many - return all sessions for this project
        IEnumerable<Session> GetSessions(int id);
    }
}
