﻿using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Interfaces
{
    public interface ICommentDetail
    {
        CommentDetail GetById(int CommentId, int SessionId);
        IEnumerable<CommentDetail> GetAll();

        void Add(CommentDetail newCommentDetail);
        void Remove(int CommentId, int SesionId);
    }
}
