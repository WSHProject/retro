﻿using Retro.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Retro.Data.Interfaces
{
    public interface IUser
    {
        User GetById(int id);
        IEnumerable<User> GetAll();
        void Add(User newUser);
        void Remove(int id);

        string GetLogin(int id);
        string GetPassword(int id);
        string GetFirstName(int id);
        string GetLastName(int id);
        string GetEmailAddress(int id);
        bool GetActive(int id);
        void SetActive(int id, bool status);
        void SetPassword(int id, string password);
        string GetAvatarUrl(int id);

        // return all comments for this user
        IEnumerable<Comment> GetComments(int id);

        // return a collection of session details for this user
        IEnumerable<SessionDetail> GetSessionDetails(int id);
    }
}
